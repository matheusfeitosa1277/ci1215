#include "queue.h"
#include <stdio.h>

int queue_size(queue_t *queue) {
    int count = 0;
    queue_t *aux = queue;

    if (queue) 
        do {
            ++count;
            queue = queue->next;
        } while (aux != queue); /* Completou o Ciclo */

    return count;
}

void queue_print(char *name, queue_t *queue, void print_elem (void*)) {
    queue_t *aux = queue;

    putchar('[');

    if (queue)
        do {
            print_elem(queue); putchar(' ');
            queue = queue->next;
        } while (aux != queue); /* Completou o Ciclo */

    puts("]");
}

int queue_append(queue_t **queue, queue_t *elem) { /* Insere um elemento no final da fila */

    // - a fila deve existir ???

    if (!elem || elem->prev) /* elemento nao existe || esta em outra lista */
        return -1;

    if (*queue) {
        (*queue)->prev->next = elem; /* Manipulacao tail */

        elem->prev = (*queue)->prev; 
        elem->next = (*queue);

        (*queue)->prev = elem;       /* Manipulacao head */
    }

    else { /* Fila Vazia */
        (*queue) = elem;

        elem->prev = elem;
        elem->next = elem;
    }

    return 0;
}

int queue_remove(queue_t **queue, queue_t *elem) { /* Remove o elemento indicado */

    if (!*queue) /* a fila deve existir */
        return -1;

    queue_t *aux = *queue;
    int in = 0;

    if (elem) /* o elemento deve existir */
        do {
            if (aux == elem) {
                in = 1; /* o elemento deve pertencer a fila indicada */
                break;
            }
            aux = aux->next;
        } while (*queue != aux);
    
    if (!in) 
        return -2;

    elem->prev->next = elem->next;
    elem->next->prev = elem->prev;

    if (elem == (*queue)) // Remocao head
        (*queue) = (*queue)->next;    

    elem->prev = NULL;
    elem->next = NULL;

    if ((*queue)->next == NULL) // Remocao ultimo elemento 
        (*queue) = NULL;

    return 0;
}

