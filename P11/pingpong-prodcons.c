#include <stdio.h>
#include <stdlib.h>
#include "ppos.h"

/* Esse codigo ta nojeto... Sem tempo pra arrumar agr... */

task_t  task_consumidor, task_produtor;
task_t  task_consumidor2, task_produtor2, task_produtor3;
semaphore_t semaphore_item, semaphore_buffer, semaphore_vaga;

typedef struct buffer{
    int array[5];
    int read;
    int write;
} buffer;

buffer buffer_fifo;

// corpo da thread A
void produtor (void * arg) {

    for (;;) {
        task_sleep(1000);

        sem_down(&semaphore_vaga);
        
        sem_down(&semaphore_buffer);

        buffer_fifo.write = (buffer_fifo.write + 1);
        buffer_fifo.array[buffer_fifo.write % 5] = random() % 100;
        printf ("%d | %s produziu %d\n", buffer_fifo.read,(char *) arg, buffer_fifo.array[buffer_fifo.write % 5]);

        sem_up(&semaphore_buffer) ;

        sem_up(&semaphore_item);
   }
}

// corpo da thread B
void consumidor (void * arg) {
    int aux;

    for (;;) {
        sem_down(&semaphore_item);

        sem_down(&semaphore_buffer);
    
        buffer_fifo.read = (buffer_fifo.read + 1);
        aux = buffer_fifo.array[buffer_fifo.read % 5];

        sem_up(&semaphore_buffer);

        sem_up(&semaphore_vaga);

        printf ("\t\t\t\t %d | %s consumiu %d\n", buffer_fifo.read, (char *) arg, aux) ;

        task_sleep(1000);
   }
}

int main (int argc, char *argv[])
{
   printf ("main: inicio\n") ;

   ppos_init () ;

   buffer_fifo.read = -1;
   buffer_fifo.write= -1;

   // inicia semaforos
   sem_init (&semaphore_item, 0) ; /* Quantidade de itens no buffer */
   sem_init (&semaphore_buffer, 1) ;
   sem_init (&semaphore_vaga, 5) ;

   srandom(42);

   // inicia tarefas
   task_init (&task_consumidor, consumidor, "c1") ;
   task_init (&task_produtor, produtor, "p1") ;
   task_init (&task_produtor2, produtor, "p2") ;
   task_init (&task_produtor3, produtor, "p3") ;
   task_init (&task_consumidor2, consumidor, "c2") ;
   /* 
   task_init (&a2, TaskA, "\tA2") ;
   task_init (&b1, TaskB, "\t\t\tB1") ;
   task_init (&b2, TaskB, "\t\t\t\tB2") ;

   // aguarda a1 encerrar
   task_wait (&a1) ;

   // destroi semaforos
   sem_destroy (&s1) ;
   sem_destroy (&s2) ;

   // aguarda a2, b1 e b2 encerrarem
   task_wait (&a2) ;
   task_wait (&b1) ;
   task_wait (&b2) ;
   */

   printf ("main: fim\n") ;
   task_exit (0) ;
}
