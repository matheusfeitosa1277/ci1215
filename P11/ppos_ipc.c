#include "ppos.h"

extern task_t *task_current;
extern task_t *queue_task_ready;
extern task_t task_dispatcher;

/* --- P10 Construcao de semaforos --- */

int sem_init (semaphore_t *s, int value) {
    if (!s)
        return -1;

    s->queue = NULL;
    s->value = value;
    s->lock = 0;

    return 0;
}


int sem_down(semaphore_t *s) {
    if (!s) 
        return -1; // Semaforo nao existe

    /* Regiao Atomica */

    while (__sync_fetch_and_or (&s->lock, 1));


    if (s->value <= 0) {
        s->lock = 0;
        task_suspend(&s->queue);
    }

    --s->value;

    s->lock = 0;
    /* --- --- */
    
    return 0;
}

int sem_up(semaphore_t *s) {
    if (!s) 
        return -1; // Semaforo nao existe

    /* Regiao Atomica */

    while (__sync_fetch_and_or (&s->lock, 1));

    ++s->value;

    task_awake(s->queue, &s->queue);    /* Acorda a proxima que liberou */

    s->lock = 0;

    /* --- --- */

    return 0;
}

int sem_destroy(semaphore_t *s) {
    if (!s)
        return -1;

    /* Regioa Atomica */
    while (__sync_fetch_and_or(&s->lock, 1));

    while (s->queue)
        task_awake(s->queue, &s->queue);

    s->lock = 0;

    s = NULL; 

    /* --- --- */

    return 0;
}


/* --- --- */

int mqueue_init(mqueue_t *queue, int max_msgs, int msg_size) {
    return 0;
}

int mqueue_send(mqueue_t *queue, void *msg) {
    return 0;
}

int mqueue_recv(mqueue_t *queue, void *msg) {
    return 0;
}

int mqueue_destroy(mqueue_t *queue) {
    return 0;
}

int mqueue_msgs(mqueue_t *queue) {
    return 0;
}

