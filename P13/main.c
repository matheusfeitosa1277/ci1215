#include <stdio.h>
#include <stdlib.h>
#include "ppos.h"
#include "ppos_disk.h"

int main() {
    ppos_init();

    int numBlocks, blockSize;
    char buffer[1024];

    disk_mgr_init(&numBlocks, &blockSize);

    puts("Starting");

    int i = 0;
    while (1) {
        printf("Requesting Block %d\n", i);
        disk_block_read(i, &buffer);

        printf("Leitura do Bloco %d: %s\n", i++, buffer);
        //disk_block_write(1, buffer);
    }


    task_exit(0);
}
