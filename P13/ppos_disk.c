#include "ppos_disk.h"
#include "disk.h"
#include "ppos_data.h"
#include "ppos.h"

disk_t file_disk;             /* Estrutura representando o disco */

task_t task_disk;             /* thread gerente de disco */

disk_request *queue_disk;     /* Fila de pedidos do disco */

task_t *queue_disk_waiting;   /* Fila que os processo descansam */

struct sigaction disk_action; /* Tratador de interrupcoes do disco */

void disk_signal_handler(int signum) {
    printf("Operacao concluida\n");

    file_disk.signal = 1; /* Gerente precisa olhar */

    task_awake(&task_disk, &queue_disk_waiting);
}

void func_disk() {

    while (1) {

        sem_down(file_disk.s);

        printf("Signal %d\n", file_disk.signal);

        if (file_disk.signal == 1) {
            printf("Liberando Pedido \n");
            task_awake(queue_disk_waiting, &queue_disk_waiting); /* FIFO */

            printf("Proximo Pedido: %p\n", queue_disk_waiting);

            file_disk.signal = 0;
        }

        int aux = disk_cmd(DISK_CMD_STATUS, 0, 0);

        printf("Disk Status: %d\n", aux);

        if (aux == DISK_STATUS_IDLE && queue_disk) {
            printf("Solicitando o proximo: "); 

            printf("%d %d %p\n", queue_disk->type, queue_disk->block, queue_disk->buffer);

            disk_cmd(queue_disk->type, queue_disk->block, queue_disk->buffer);

            queue_remove((queue_t **) &queue_disk, (queue_t *) queue_disk); /* Remove o pedido */ /* Nao ta funcionando */
         }
        sem_up(file_disk.s);

        puts("Dormindo");

        task_suspend(&queue_disk_waiting); /* Quando precisarem eu volto */
    }
}

int disk_mgr_init(int *numBlocks, int *blockSize) { /* Sincrono */

    if (disk_cmd(DISK_CMD_INIT, 0, 0)) /* Erro na inicializacao do disco */
        return -1;

    disk_action.sa_handler = disk_signal_handler; /* ideia base */
    sigemptyset(&disk_action.sa_mask);
    disk_action.sa_flags = 0;
    sigaction(SIGUSR1, &disk_action, 0);

    (*blockSize) = disk_cmd(DISK_CMD_BLOCKSIZE, 0, 0);
    (*numBlocks) = disk_cmd(DISK_CMD_DISKSIZE, 0, 0);

    queue_disk_waiting = NULL;
    queue_disk = NULL;

    file_disk.signal = 0;

    task_init(&task_disk, (void *)(*func_disk), NULL);

    return 0;
}

int disk_block_read(int block, void *buffer) { /* Assincrono */
    disk_request read_request;

    read_request.prev = NULL;
    read_request.next = NULL;
    read_request.type = DISK_CMD_READ;
    read_request.block = block;
    read_request.buffer = buffer;
    
    sem_down(file_disk.s);

        printf("%p Pedido: %d %d %p\n", &read_request, read_request.type, read_request.block, read_request.buffer);

        queue_append((queue_t **) &queue_disk, (queue_t *) &read_request); /* Enfilera o pedido */

        task_awake(&task_disk, &queue_disk_waiting); /* Acorda o gerente */

    sem_up(file_disk.s);

    task_suspend(&queue_disk_waiting); /* Dormir ate retornarem meu pedido */

    return 0;
}

//  int disk_block_write(int block, void *buffer) {

//      if (disk_cmd(DISK_CMD_WRITE, block, buffer))
//          return -1;

//      // PRECISO SER SUSPENSA, ESCRITA AGENDADA
//      //task_suspend(&queue_disk);

//      return 0;
//  }

