#include "ppos.h"
#include <string.h>

extern task_t *task_current;
extern task_t *queue_task_ready;
extern task_t task_dispatcher;

/* --- P10 Construcao de semaforos --- */

int sem_init (semaphore_t *s, int value) {
    if (!s)
        return -1;

    s->queue = NULL;
    s->value = value;
    s->lock = 0;
    s->destroyed = 0;

    return 0;
}


int sem_down(semaphore_t *s) {
    if (!s || s->destroyed) 
        return -1; // Semaforo nao existe

    /* Regiao Atomica */

    while (__sync_fetch_and_or (&s->lock, 1));


    if (s->value <= 0) {
        s->lock = 0;
        task_suspend(&s->queue);
    }

    --s->value;

    s->lock = 0;
    /* --- --- */
    
    return 0;
}

int sem_up(semaphore_t *s) {
    if (!s || s->destroyed) 
        return -1; // Semaforo nao existe

    /* Regiao Atomica */

    while (__sync_fetch_and_or (&s->lock, 1));

    ++s->value;

    task_awake(s->queue, &s->queue);    /* Acorda a proxima que liberou */

    s->lock = 0;

    /* --- --- */

    return 0;
}

int sem_destroy(semaphore_t *s) {
    if (!s || s->destroyed)
        return -1;

    /* Regioa Atomica */
    while (__sync_fetch_and_or(&s->lock, 1));

    while (s->queue)
        task_awake(s->queue, &s->queue);

    s->lock = 0;
    s->destroyed = 1;

    s = NULL; 

    /* --- --- */

    return 0;
}

/* --- P12 Filas De Mensagens --- */

int mqueue_init(mqueue_t *queue, int max_msgs, int msg_size) {
    void *error_check = 
        malloc(sizeof (char) * (max_msgs * msg_size) ); 

    if (error_check) {
        queue->max_msgs = max_msgs;
        queue->msg_size = msg_size;
        queue->num_msgs = 0;

        sem_init(&queue->s_item, 0);
        sem_init(&queue->s_buffer, 1);
        sem_init(&queue->s_slot, max_msgs);

        queue->read = -1;
        queue->write = -1;
        queue->buffer = error_check;

        return 0;
    }

    return -1;
}

int mqueue_send(mqueue_t *queue, void *msg) {

    if (sem_down(&queue->s_slot)) { puts("rodei"); return -1;}

    if (sem_down(&queue->s_buffer)) return -1;

    int position = (++queue->write * queue->msg_size) % (queue->max_msgs * queue->msg_size);
    memcpy(&queue->buffer[position], msg, queue->msg_size);
    ++queue->num_msgs;

    if (sem_up(&queue->s_buffer)) return -1;

    if (sem_up(&queue->s_item)) return -1;

    return 0;
}

int mqueue_recv(mqueue_t *queue, void *msg) {

    if (sem_down(&queue->s_item)) return -1;

    if (sem_down(&queue->s_buffer)) return -1;

    int position = (++queue->read * queue->msg_size) % (queue->max_msgs * queue->msg_size);
    memcpy(msg, &queue->buffer[position], queue->msg_size);
    --queue->num_msgs;

    if (sem_up(&queue->s_buffer)) return -1;

    if (sem_up(&queue->s_slot)) return -1;

    return 0;
}

int mqueue_destroy(mqueue_t *queue) { /* Incompleto */
    if (!queue)
        return -1;

    sem_destroy(&queue->s_item);
    sem_destroy(&queue->s_buffer);
    sem_destroy(&queue->s_slot);

    if (queue->buffer) {
        free(queue->buffer);
        queue = NULL;
        return 0;
    }

    return 1;
}

int mqueue_msgs(mqueue_t *queue) {
    return queue->num_msgs;
}

