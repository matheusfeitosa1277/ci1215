#include "ppos.h"

// Variaveis Globais do Kernel
int ID;
task_t task_main;
task_t *task_atual;

void ppos_init() {
    ID = 0;
    setvbuf(stdout, NULL, _IONBF, 0); /* Fecha stdout */

    task_main.id = ID++;            // Main id = 0
    task_atual = &task_main;

    #ifdef DEBUG
        printf("ppos_init: inicializado com sucesso\n");
    #endif
}


int task_init(task_t *task, void (*start_routine)(void *), void (*arg)) {
    getcontext(&task->context);
    task->id = ID++;

    printf("%p\n", task);

    #ifdef DEBUG
        printf("task_init: iniciando tarefa %d\n", task->id);
    #endif 

        task->context.uc_stack.ss_sp = malloc(UCONTEXT_STACKSIZE); /* Criar uma pilha pra thread */
        task->context.uc_stack.ss_size = UCONTEXT_STACKSIZE;
        task->context.uc_stack.ss_flags = 0;
        task->context.uc_link = 0; /* Man sugere um addr */
    makecontext(&task->context, (void *)(*start_routine), 1, arg);

    int *aux = (task->context.uc_stack.ss_sp);
    printf("Malloc %p\n", aux);

    return 0;
}

int task_switch(task_t *task) {
    #ifdef DEBUG
        printf("task_switch: trocando contexto %d -> %d\n", task_atual->id, task->id);
    #endif 


    task_t *old = task_atual;

    task_atual = task;

    printf("%p | %p\n", old, task);

    swapcontext(&old->context, &task->context);

    return 0;
}

void task_exit(int exit_code) { 
    #ifdef DEBUG
        printf("task_exit: tarefa %d sendo encerrada\n", task_atual->id);
    #endif

    if (task_atual->id != 0) {
        int *aux = (task_atual->context.uc_stack.ss_sp);
        printf("Free %p\n", aux);
        task_switch(&task_main); /* Falta liberar memoria... */
    }
}

int task_id() {
    return task_atual->id;
}
