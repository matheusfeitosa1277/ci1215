#include "ppos.h"

/* Variaveis Globais do Kernel */

//P2 - Gestao de tarefas
int ID;
task_t task_main;
task_t *task_current;

//P3 - Dispatcher
void dispatcher();
task_t* scheduler();
void task_yield();

task_t task_dispatcher;
task_t *queue_task_ready;

//P4 - Escalonamento por prioridades
void task_setprio (task_t *task, int prio); 
int task_getprio(task_t *task); 

void ppos_init() {
    ID = 0;

    //setvbuf(stdout, NULL, _IONBF, 0); /* Fecha stdout */

    task_main.id = ID++;            // Main id = 0
    task_current = &task_main;

    queue_task_ready = NULL;
    task_init(&task_dispatcher, (void *)(*dispatcher), NULL); 

    #ifdef DEBUG
        printf("ppos_init: inicializado com sucesso\n");
    #endif
}


int task_init(task_t *task, void (*start_routine)(void *), void (*arg)) {
    task->id = ID++;

    #ifdef DEBUG
        printf("task_init: iniciando tarefa %d\n", task->id);
    #endif 

    task->prev = NULL;
    task->next = NULL;

    queue_append((queue_t **) &queue_task_ready, (queue_t *) task);

    task->static_prio = 0;
    task->dinamic_prio = 0;

    getcontext(&task->context);
        task->context.uc_stack.ss_sp = malloc(UCONTEXT_STACKSIZE);  /* Criar uma pilha pra thread */
        task->context.uc_stack.ss_size = UCONTEXT_STACKSIZE;
        task->context.uc_stack.ss_flags = 0;
        task->context.uc_link = 0;                                  /* Man sugere um addr */
    makecontext(&task->context, (void *)(*start_routine), 1, arg);

    return 0;
}

int task_switch(task_t *task) {
    #ifdef DEBUG
        printf("task_switch: trocando contexto %d -> %d\n", task_current->id, task->id);
    #endif 

    task_t *old = task_current;

    task_current = task;

    swapcontext(&old->context, &task_current->context);

    return 0;
}

void task_exit(int exit_code) { 
    #ifdef DEBUG
        printf("task_exit: tarefa %d sendo encerrada\n", task_current->id);
    #endif

    if (task_current->id) /* Se nao for a main, libera a pilha */
        free(task_current->context.uc_stack.ss_sp);

    if (task_current->id != 1) /* se nao for o dispatcher, desvia para ele */
        task_switch(&task_dispatcher);
}

int task_id() {
    return task_current->id;
}

void dispatcher() {
    task_t *next;

    queue_remove((queue_t **) &queue_task_ready, (queue_t *) &task_dispatcher);

    while (queue_size((queue_t *) queue_task_ready) > 0) { 
        next = scheduler();

        #ifdef DEBUG
            printf("dispatcher: next task: %d\n", next->id);
        #endif

        queue_remove((queue_t **) &queue_task_ready, (queue_t *) next);

        task_switch(next);
    }


    #ifdef DEBUG
        puts("dispatcher: fila vazia, encerrando");
    #endif

    task_exit(0);
}

task_t* scheduler() {
    task_t *aux = queue_task_ready;

    task_t *min_value = queue_task_ready;

    do {
        if (min_value->dinamic_prio >= aux->dinamic_prio--)
            min_value = aux;
        

        if (aux->dinamic_prio < -20) aux->dinamic_prio = -20;

        #ifdef DEBUG
            printf("%d %d\n", aux->id, aux->dinamic_prio);
        #endif

        aux = aux->next;
    } while (aux != queue_task_ready);

    #ifdef DEBUG
        printf("min: %d %d\n", min_value->id, min_value->dinamic_prio);
    #endif

    min_value->dinamic_prio = min_value->static_prio;

    return min_value;
}

void task_yield() {
    #ifdef DEBUG
        printf("task_yield: tarefa %d dormindo\n", task_current->id);
    #endif

    queue_append((queue_t **) &queue_task_ready, (queue_t *) task_current);

    task_switch(&task_dispatcher);
}


void task_setprio (task_t *task, int prio) {
    if (!task) /* socm: Caso task seja nulo, ajusta a prioridade da tarefa atual. */
        task = task_current;
    
    if (prio > 20)
        prio = 20;
    else if (prio < -20)
        prio = -20;

    task->static_prio = prio;
    task->dinamic_prio = prio;
}

int task_getprio (task_t *task) {
    if (!task) /* socm: ... (ou da tarefa corrente, se task for nulo) */
        task = task_current;

    return task->static_prio;
}

