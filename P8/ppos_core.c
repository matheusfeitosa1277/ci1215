#include "ppos.h"

/* Variaveis Globais do Kernel */

//P2 - Gestao de tarefas
int ID;
task_t task_main;
task_t *task_current;

//P3 - Dispatcher
void dispatcher();
task_t* scheduler();
void task_yield();

task_t task_dispatcher;
task_t *queue_task_ready;

//P4 - Escalonamento por prioridades
void task_setprio (task_t *task, int prio); 
int task_getprio(task_t *task); 


//P5 - Preempcao e Compartilhamento de tempo
struct sigaction action;
struct itimerval timer;
void handler(int signum);

//P6 - Contabilizacao de tarefas
unsigned int clock_tick;
unsigned int systime();

//P8 - Tarefas Suspensas
int task_wait (task_t *task);
task_t *queue_task_waiting;
unsigned int user_task_count;
void task_suspend(task_t **queue);
void task_awake(task_t *task, task_t **queue);
int EXIT_CODE;

unsigned int systime() {
    return clock_tick;
}


/* --- P0: Gestao de Tarefas --- */

void ppos_init() {
    ID = 0;             
    user_task_count = 0;

    //setvbuf(stdout, NULL, _IONBF, 0); /* Fecha stdout */

    task_main.id = ID++;            // Main id = 0
    task_current = &task_main;

    queue_task_ready = NULL;
    queue_task_waiting = NULL;

    task_init(&task_dispatcher, (void *)(*dispatcher), NULL); 

    clock_tick = 0;

    action.sa_handler = handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGALRM, &action, 0);

    timer.it_value.tv_usec = 1000;
    timer.it_value.tv_sec = 0;
    timer.it_interval.tv_usec = 1000;
    timer.it_interval.tv_sec = 0;

    setitimer(ITIMER_REAL, &timer, 0);

    #ifdef DEBUG
        printf("ppos_init: inicializado com sucesso\n");
    #endif
}

int task_id() {
    return task_current->id;
}



int task_init(task_t *task, void (*start_routine)(void *), void (*arg)) {
    ++user_task_count;

    task->prev = NULL;
    task->next = NULL;
    task->id = ID++;

    #ifdef DEBUG
        printf("task_init: iniciando tarefa %d\n", task->id);
    #endif 

    getcontext(&task->context);
        task->context.uc_stack.ss_sp = malloc(UCONTEXT_STACKSIZE);  /* Criar uma pilha pra thread */
        task->context.uc_stack.ss_size = UCONTEXT_STACKSIZE;
        task->context.uc_stack.ss_flags = 0;
        task->context.uc_link = 0;                                  /* Man sugere um addr */
    makecontext(&task->context, (void *)(*start_routine), 1, arg);

    task->status = 0;

    task->static_prio = 0;
    task->dinamic_prio = 0;
    task->quantum = 20;
    task->activations = 0;
    task->clock_init = systime();
    task->clock_processor_time = 0;
    
    task->waiting = -1; /* Nao to esperando ngm */

    queue_append((queue_t **) &queue_task_ready, (queue_t *) task);

    return 0;
}

int task_switch(task_t *task) {
    #ifdef DEBUG
        printf("task_switch: trocando contexto %d -> %d\n", task_current->id, task->id);
    #endif 

    ++task_current->activations;

    task_t *old = task_current;

    task_current = task;

    swapcontext(&old->context, &task_current->context);

    return 0;
}



/* --- P3: Dispachante de tarefas --- */

void dispatcher() {
    task_t *next;

    queue_remove((queue_t **) &queue_task_ready, (queue_t *) &task_dispatcher);

    while (user_task_count) { 
        next = scheduler();

        #ifdef DEBUG
            printf("dispatcher: next task: %d\n", next->id);
        #endif

        queue_remove((queue_t **) &queue_task_ready, (queue_t *) next);

        task_switch(next);
    }


    #ifdef DEBUG
        puts("dispatcher: fila vazia, encerrando");
    #endif

    task_exit(0);
}

void task_yield() {
    #ifdef DEBUG
        printf("task_yield: tarefa %d dormindo\n", task_current->id);
    #endif

    queue_append((queue_t **) &queue_task_ready, (queue_t *) task_current);

    task_switch(&task_dispatcher);
}

/* --- --- */

task_t* scheduler() {
    task_t *aux = queue_task_ready;

    task_t *min_value = queue_task_ready;

    do {
        if (min_value->dinamic_prio >= aux->dinamic_prio--)
            min_value = aux;

        if (aux->dinamic_prio < -20) aux->dinamic_prio = -20;

        #ifdef DEBUG
            printf("%d %d\n", aux->id, aux->dinamic_prio);
        #endif

        aux = aux->next;
    } while (aux != queue_task_ready);

    #ifdef DEBUG
        printf("min: %d %d\n", min_value->id, min_value->dinamic_prio);
    #endif

    min_value->dinamic_prio = min_value->static_prio;

    return min_value;
}


/* --- --- */

void task_setprio (task_t *task, int prio) {
    if (!task) /* socm: Caso task seja nulo, ajusta a prioridade da tarefa atual. */
        task = task_current;
    
    if (prio > 20)
        prio = 20;
    else if (prio < -20)
        prio = -20;

    task->static_prio = prio;
    task->dinamic_prio = prio;
}

int task_getprio (task_t *task) {

    if (!task) /* socm: ... (ou da tarefa corrente, se task for nulo) */
        task = task_current;

    return task->static_prio;
}

/* --- --- */

void handler(int signum) { /* Tratador de sinais */

    ++clock_tick;

    ++task_current->clock_processor_time;

    if (task_current->id > 1) { /* User Task */
        --task_current->quantum;

        if (task_current->quantum < 1) { /* End of quantum */
            task_current->quantum = 20;
            task_yield(task_current);
        }
    }

};

/* --- P8: Tarefas Suspensas --- */

void task_suspend(task_t **queue) {

    queue_remove((queue_t **) &queue_task_ready, (queue_t *) task_current); /* -2 always */

    task_current->status = 1;

    queue_append((queue_t **) queue, (queue_t *) task_current);

    task_switch(&task_dispatcher);
}

void task_awake(task_t *task, task_t **queue) {
    queue_remove((queue_t **) &queue_task_waiting, (queue_t *) task);
     
    task_current->status = 0;

    queue_append((queue_t **) &queue_task_ready, (queue_t *) task);
}

int task_wait(task_t *task) {
    if (!task) 
        return -1;

    #ifdef DEBUG
        printf("task_wait: task %d waiting for %d\n", task_current->id, task->id);
    #endif

    task_current->waiting = task->id;

    task_suspend(&queue_task_waiting);

    return EXIT_CODE;
}


void task_exit(int exit_code) { 
    --user_task_count;

    #ifdef DEBUG
        printf("task_exit: tarefa %d sendo encerrada\n", task_current->id);
    #endif

    printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n", 
        task_current->id, systime() - task_current->clock_init, 
        task_current->clock_processor_time, task_current->activations);

    /* Libera as tarefas esperando */
    queue_t *aux = (queue_t *) queue_task_waiting;
    task_t *task_waiting;

    mark:

    if (queue_task_waiting)
        do {
            task_waiting = (task_t *) aux;


            if (task_waiting->waiting == task_current->id) {
                #ifdef DEBUG
                    printf("%d | Task %d Waiting: %d\n", 
                        task_current->id, task_waiting->id, task_waiting->waiting);

                    puts("eu deveria me acordar");
                #endif

                task_awake(task_waiting, &queue_task_waiting);

                goto mark;
            }

            aux = aux->next; 
        } while (aux !=  (queue_t *) queue_task_waiting);

    /* --- --- */

    EXIT_CODE = exit_code;

    if (task_current->id) /* Se nao for a main, libera a pilha */
        free(task_current->context.uc_stack.ss_sp);

    if (task_current->id != 1) /* se nao for o dispatcher, desvia para ele */
        task_switch(&task_dispatcher);
}

