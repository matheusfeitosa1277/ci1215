#include "ppos.h"

extern task_t *task_current;

/* --- P10 Construcao de semaforos --- */

int sem_init (semaphore_t *s, int value) {
    if (!s)
        return -1;

    s->queue = NULL;
    s->value = value;
    s->lock = 0;

    return 0;
}


int sem_down(semaphore_t *s) {
    if (!s) 
        return -1; // Semaforo nao existe

    /* Regiao Atomica */

    while (__sync_fetch_and_or (&s->lock, 1));

    //printf("task %d Locking %p\n", task_current->id, s);

    --s->value;

    //printf("tentando %d\n", task_current->id);

    if (s->value < 0) {
        //printf("   dormindo %d\n", task_current->id);
        s->lock = 0;
        task_suspend(&s->queue);
    }


    s->lock = 0;
    /* --- --- */
    
    return 0;
}

int sem_up(semaphore_t *s) {
    if (!s) 
        return -1; // Semaforo nao existe

    /* Regiao Atomica */

    while (__sync_fetch_and_or (&s->lock, 1));

    //printf("task %d Unlocking %p\n", task_current->id, s);

    ++s->value;

    task_awake(s->queue, &s->queue);    /* Acorda a proxima que liberou */

    s->lock = 0;

    //printf("%d acordando %d\n", task_current->id, s->queue->id);

    /* --- --- */

    return 0;
}

int sem_destroy(semaphore_t *s) {
    if (!s)
        return -1;

    /* Regioa Atomica */
    while (__sync_fetch_and_or(&s->lock, 1));

    while (s->queue)
        task_awake(s->queue, &s->queue);

    s->lock = 0;

    s = NULL; 

    /* --- --- */

    return 0;
}

